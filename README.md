## Recommender Systems Competition, PoliMi academic year 2015/2016 1st semester. ##

**Team Name:** "Cinque Lire"

**Team Members:** Paolo Guglielmino, Alessandro Negrini

Goal of the project: Create a recommender system for movies, which recommends to a user 5 different movies that he/she has not already watched.

The competition is hosted on Kaggle: https://inclass.kaggle.com/c/recommender-system-2015-challenge-polimi

The dataset is in the folder data/
The files provided for the competition are 4:

* train.csv (Contains the train set which is a User Rating Matrix of the form: userId,movieId,Rating)

* icm.csv (Contains the feature of each movie in the form: movieId,featureId)

* test.csv (Contains the users which are part of the test set)

* samplesubmission.csv (An example which tells how the output file should be structured)

N.B. Other csv files are auxiliary data sets created by the algorithms in the project in order to save memory and computational time.

### How do I get set up? ###

Just open the project in an IDE such as NetBeans or Eclipse and choose your favorite algorithm in the main class. Pay attention to the cvs files that are needed as input by the algorithm.

Results are saved as cvs files comma delimited in data/results/