/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.recsys.recsys2k15.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paologuglielmino
 */
public class Utility {

    public static List<String[]> readCsv(String pathToFile) {
        List<String[]> parsedCsvFile = new ArrayList();

        BufferedReader br;
        String line;
        String csvSplitBy = "[^E0123456789.-]";
        String[] csvLine;

        try {
            br = new BufferedReader(new FileReader(pathToFile));
            while ((line = br.readLine()) != null) {
                csvLine = line.split(csvSplitBy);
                parsedCsvFile.add(csvLine);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parsedCsvFile;
    }

    /**
     * Simple Similarity sim = numAttributiInComuneNeiDueFilm /
     * numTotaleDegliAttributi
     *
     * @param vecA mappa degli attributi del primo film
     * @param vecB mappa degli attributi del secondo film
     * @return
     */
    public static double simpleSimilarity(Map<String, Double> vecA, Map<String, Double> vecB) {

        double valueAttributiInComune = 0;
        int numAttributiInComune = 0;
        double numTotaleAttributi = 0;

        //Conto gli attributi in comune andando a scorrere la mappa del primo film
        // e guardo se tale attributo è contenuto anche nel secondo -> incremento 'numAttributiInComune'
        for (Map.Entry<String, Double> entry : vecA.entrySet()) {
            if (vecB.containsKey(entry.getKey())) {
                numAttributiInComune++;
                valueAttributiInComune += entry.getValue();
            }
        }

        //Ora calcolo il numero totale di attributi
        numTotaleAttributi = vecA.size() + vecB.size() - numAttributiInComune;

        return valueAttributiInComune / (numTotaleAttributi + 7);

    }

    public static double CosineSimilarity(Map<String, Double> vecA, Map<String, Double> vecB, int shrink) {
        if (vecA == null || vecB == null) {
            return 0;
        }
        double dotProduct = DotProduct(vecA, vecB);
        double magnitudeOfA = Magnitude(vecA);
        double magnitudeOfB = Magnitude(vecB);

        return dotProduct / (magnitudeOfA * magnitudeOfB + shrink);
    }

    public static double DotProduct(Map<String, Double> vecA, Map<String, Double> vecB) {
        double dotProduct = 0;
        for (Map.Entry<String, Double> entry : vecA.entrySet()) {
            if (vecB.containsKey(entry.getKey())) {
                dotProduct = dotProduct + entry.getValue() * vecB.get(entry.getKey());
            }
        }
        return dotProduct;
    }

    public static double Magnitude(Map<String, Double> vector) {
        return Math.sqrt(DotProduct(vector, vector));
    }

    public static void fillFinalCsv(String pathToCsvToFill, String pathToFinalResult, String[] fillers) throws IOException {
        FileWriter writer = new FileWriter(pathToFinalResult);
        List<String[]> rawCsv = readCsv(pathToCsvToFill);
        for (String[] csvLine : rawCsv) {
            System.out.println(csvLine.length);
            if (csvLine[0].equals("userId")) {
                writer.append(csvLine[0] + "," + csvLine[1]);
            } else {
                for (int i = 0; i < csvLine.length; i++) {
                    writer.append(csvLine[i]);
                    if (i == 0) {
                        writer.append(",");
                    } else {
                        writer.append(" ");
                    }
                }
                int j = 0;
                for (int i = csvLine.length; i <= 5; i++) {
                    writer.append(fillers[j] + " ");
                    j++;
                }
            }
            writer.write(System.getProperty("line.separator"));
        }
        writer.flush();
        writer.close();
    }

    public static <K, V extends Comparable<? super V>> List<Map.Entry<K, V>>
            findGreatest(Map<K, V> map, int n) {
        Comparator<? super Map.Entry<K, V>> comparator
                = new Comparator<Map.Entry<K, V>>() {
                    @Override
                    public int compare(Map.Entry<K, V> e0, Map.Entry<K, V> e1) {
                        V v0 = e0.getValue();
                        V v1 = e1.getValue();
                        return v0.compareTo(v1);
                    }
                };
        PriorityQueue<Map.Entry<K, V>> highest;
        highest = new PriorityQueue<>(n, comparator);
        for (Map.Entry<K, V> entry : map.entrySet()) {
            highest.offer(entry);
            while (highest.size() > n) {
                highest.poll();
            }
        }

        List<Map.Entry<K, V>> result = new ArrayList<>();
        while (highest.size() > 0) {
            result.add(highest.poll());
        }
        return result;
    }
}
