/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.recsys.recsys2k15.model;

/**
 *
 * @author paologuglielmino
 */
public class UrmRow {

    private String userId;
    private String itemId;
    private int rating;

    public UrmRow(String userId, String itemId, String rating) {
        this.userId = userId;
        this.itemId = itemId;
        this.rating = Integer.parseInt(rating);
    }

    public String getUserId() {
        return userId;
    }

    public String getItemId() {
        return itemId;
    }

    public int getRating() {
        return rating;
    }
}
