/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.recsys.recsys2k15.model;

/**
 *
 * @author paologuglielmino
 */
public class ItemRating {

    private String itemId;
    private double predictedRating;

    public ItemRating(String itemId, double predictedRating) {
        this.itemId = itemId;
        this.predictedRating = predictedRating;
    }

    public String getItemId() {
        return itemId;
    }

    public double getPredictedRating() {
        return predictedRating;
    }
}
