/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.recsys.recsys2k15.model;

/**
 *
 * @author paologuglielmino
 */
public class SimilarCouple {

    private String el1;
    private String el2;
    private double similarity;

    public SimilarCouple(String item1, String item2, double similarity) {
        this.el1 = item1;
        this.el2 = item2;
        this.similarity = similarity;
    }

    public String getEl1() {
        return el1;
    }

    public String getEl2() {
        return el2;
    }

    public double getSimilarity() {
        return similarity;
    }
}
