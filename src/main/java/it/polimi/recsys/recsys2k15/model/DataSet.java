/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.recsys.recsys2k15.model;

import java.util.ArrayList;
import java.util.List;
import it.polimi.recsys.recsys2k15.utils.Utility;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author paologuglielmino
 */
public class DataSet {

    //Parameters
    //User Based
    public final int COSINE_SHRINK_USERSIM = 72;
    public final int PEARSON_SHRINK_USERSIM = 60;
    public final int PREDICTED_SHRINK_UBF = 10;
    public final int KNN = 150;
    //Content Based
    public final int PREDICTED_SHRINK_CBF = 4;
    public final int COSINE_SHRINK = 0;
    public final int CBF_SHRINK = 3;
    public final int RATING_THRESHOLD = 6;
    public final double SIMILARITY_THRESHOLD = 0.1;
    //Top Average
    public final int AVG_SHRK = 7;
    //Top Popular
    public final int TOP_POP = 80;

    //  userId - itemId - rating
    private List<UrmRow> urm;
    //  userId - [itemId - rating]
    private Map<String, Map<String, Integer>> fasterUrm;
    //  itemId - [userId - rating]
    private Map<String, Map<String, Integer>> fasterUrmByItems;
    //  userId - [itemIds]
    private Map<String, List<String>> watchedMovies;
    //  itemId - [featureId - 1.0]
    private Map<String, Map<String, Double>> icm;
    //  userId
    private List<String> testUsers;
    //  itemId
    private List<String> allFeaturedMovies;
    //  userId - ratings average
    private Map<String, Double> allUsersAverages;
    //  itemId - ratings average
    private Map<String, Double> allItemsAverages;
    //  userId - [userId - similarity]
    private Map<String, Map<String, Double>> userSimilarities;
    //  userId - [userId - pearson_similarity]
    private Map<String, Map<String, Double>> userPearsonSimilarities;
    //  itemId
    private List<String> nTopPop;

    private String[] topAvg = {"33173", "33475", "1076", "15743", "35300", "36872", "32347", "12866", "17648", "34517"};

    private List<Map.Entry<String, Double>> topAverages;

    public DataSet() throws IOException {
        parseURM();
        loadFasterURM("data/train_by_users.csv", true);
        loadFasterURM("data/train.csv", false);
        parseICM();
        parseTestUsers();
        computeUsersAverages();
        computeItemsAverages();

        //SIMILARITY COMPUTING, EXECUTE ONLY ONCE!
        //computeUsersSimilarities();
        //computeUsersPearsonSimilarities();
        //computeUsersSimpleSimilarities();
        
        loadUsersSimilarities();
        loadNTopPop(TOP_POP);
        useWeightedFeatures();
        System.out.println("DataSet loaded!");
    }

    private void parseURM() {
        urm = new ArrayList();
        watchedMovies = new HashMap();
        List<String[]> rawCsv = Utility.readCsv("data/train.csv");
        for (String[] csvLine : rawCsv) {
            UrmRow urmLine = new UrmRow(csvLine[0], csvLine[1], csvLine[2]);
            urm.add(urmLine);
            if (watchedMovies.containsKey(csvLine[0])) {
                watchedMovies.get(csvLine[0]).add(csvLine[1]);
            } else {
                watchedMovies.put(csvLine[0], new ArrayList());
                watchedMovies.get(csvLine[0]).add(csvLine[1]);
            }
        }
        System.out.println("URM parsed!");
        System.out.println("Already watched movies parsed!");
    }

    private void loadFasterURM(String pathToUrmFile, boolean loadByUsers) {
        int index = (loadByUsers) ? 0 : 1;
        int second = (loadByUsers) ? 1 : 0;
        Map<String, Map<String, Integer>> MyUrm = new HashMap();
        List<String[]> rawCsv = Utility.readCsv(pathToUrmFile);
        Map<String, Integer> temp = new HashMap();
        String last = "";
        for (String[] csvLine : rawCsv) {
            String current = csvLine[index];
            if (current.equals(last) || last.equals("")) {
                temp.put(csvLine[second], Integer.parseInt(csvLine[2]));
            } else {
                Map<String, Integer> movieRatings = new HashMap();
                for (Map.Entry<String, Integer> entry : temp.entrySet()) {
                    movieRatings.put(entry.getKey(), entry.getValue());
                }
                MyUrm.put(last, movieRatings);
                temp.clear();
                temp.put(csvLine[second], Integer.parseInt(csvLine[2]));
            }
            last = current;
        }
        MyUrm.put(last, temp);
        if (loadByUsers) {
            fasterUrm = MyUrm;
        } else {
            fasterUrmByItems = MyUrm;
        }
    }

    private void parseICM() {
        icm = new HashMap();
        allFeaturedMovies = new ArrayList();
        List<String[]> rawCsv = Utility.readCsv("data/icm.csv");
        Collections.sort(rawCsv, new Comparator<String[]>() {
            @Override
            public int compare(String[] o1, String[] o2) {
                return Integer.compare(Integer.parseInt(o1[0]), Integer.parseInt(o2[0]));
            }
        });
        String currentItem;
        String lastItem = "";
        Map<String, Double> temp = new HashMap();
        for (String[] csvLine : rawCsv) {
            currentItem = csvLine[0];
            if (lastItem.equals(currentItem) || lastItem.equals("")) {
                temp.put(csvLine[1], 1.0);
            } else {
                Map<String, Double> itemFeatures = new HashMap();
                for (Map.Entry<String, Double> entry : temp.entrySet()) {
                    itemFeatures.put(entry.getKey(), entry.getValue());
                }
                icm.put(lastItem, itemFeatures);
                if (!(itemFeatures.isEmpty())) {
                    allFeaturedMovies.add(lastItem);
                }
                temp.clear();
                temp.put(csvLine[1], 1.0);
            }
            lastItem = currentItem;
        }
        icm.put(lastItem, temp);
        if (!(temp.isEmpty())) {
            allFeaturedMovies.add(lastItem);
        }
        System.out.println("ICM parsed!");
    }

    private void parseTestUsers() {
        testUsers = new ArrayList();
        List<String[]> rawCsv = Utility.readCsv("data/test.csv");
        for (String[] csvLine : rawCsv) {
            testUsers.add(csvLine[0]);
        }
        System.out.println("Test users parsed!");
    }

    private void useWeightedFeatures() {
        Map<String, Double> featuresIdfs = new HashMap();
        Map<String, Double> featuresTfIdfs = new HashMap();

        String lastFeature = "";
        String currentFeature = "";
        int count = 0;
        double tf = 1 / 19715;
        double idf;

        List<String[]> rawCsv = Utility.readCsv("data/icm.csv");
        for (String[] csvLine : rawCsv) {
            currentFeature = csvLine[1];
            if (!(currentFeature.equals(lastFeature)) && !"".equals(lastFeature)) {
                idf = 1 + Math.log10(37142.0 / count);
                featuresIdfs.put(lastFeature, idf);
                featuresTfIdfs.put(lastFeature, tf * idf);
                count = 1;
            } else {
                count++;
            }
            lastFeature = currentFeature;
        }
        idf = 1 + Math.log10(37142.0 / count);
        featuresIdfs.put(currentFeature, idf);
        featuresTfIdfs.put(lastFeature, tf * idf);

        //Decide here how to weight the icm values
        applyWeights(featuresIdfs);
    }

    private void applyWeights(Map<String, Double> featuresWeights) {
        for (Map.Entry<String, Map<String, Double>> entry : icm.entrySet()) {
            for (Map.Entry<String, Double> entry2 : entry.getValue().entrySet()) {
                entry2.setValue(featuresWeights.get(entry2.getKey()));
            }
        }
    }

    public boolean alreadyWatched(String userId, String itemId) {
        return watchedMovies.get(userId).contains(itemId);
    }

    public int getSpecificRating(String userId, String itemId) {
        try {
            return fasterUrm.get(userId).get(itemId);
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public boolean isThisItemFeatured(String itemId) {
        return allFeaturedMovies.contains(itemId);
    }

    private void computeUsersAverages() {
        allUsersAverages = new HashMap();
        for (int i = 1; i <= 15374; i++) {
            Map<String, Integer> userRatings = fasterUrm.get("" + i);
            int sum = 0;
            if (fasterUrm.get("" + i) != null) {
                for (Map.Entry<String, Integer> entry : userRatings.entrySet()) {
                    sum += entry.getValue();
                }
            }
            double avg;
            if (fasterUrm.get("" + i) != null) {
                avg = (double) sum / userRatings.size();
            } else {
                avg = 0;
            }
            allUsersAverages.put("" + i, avg);
        }
        System.out.println("Users' averages computed!");
    }

    private List<Integer> getUserRatings(String userId) {
        List<Integer> userRatings = new ArrayList();
        for (UrmRow urmRow : urm) {
            if (urmRow.getUserId().equals(userId)) {
                userRatings.add(urmRow.getRating());
            }
        }
        return userRatings;
    }

    public double getPearsonCorrelation(String userU, String userV, int shrink) {
        List<String> coRatedItems = getCoRatedItems(userU, userV);
        if (coRatedItems == null) {
            return 0;
        }
        double numerator = 0;
        double firstTerm;
        double secondTerm;
        double den1 = 0;
        double den2 = 0;
        double denominator = 1;

        for (String itemJ : coRatedItems) {
            firstTerm = getSpecificRating(userU, itemJ) - allUsersAverages.get(userU);
            secondTerm = getSpecificRating(userV, itemJ) - allUsersAverages.get(userV);
            numerator += (firstTerm * secondTerm);
            den1 += Math.pow(firstTerm, 2);
            den2 += Math.pow(secondTerm, 2);
        }
        if (den1 != 0 && den2 != 0) {
            denominator = Math.sqrt(den1 * den2);
        }
        return numerator / (denominator + shrink);
    }

    private List<String> getCoRatedItems(String userU, String userV) {
        List<String> coRatedItems = new ArrayList();
        if (watchedMovies.get(userU) != null && watchedMovies.get(userV) != null) {
            for (String item : watchedMovies.get(userU)) {
                if (watchedMovies.get(userV).contains(item)) {
                    coRatedItems.add(item);
                }
            }
        }
        return coRatedItems;
    }

    private void computeUsersSimilarities() throws IOException {
        FileWriter writer = new FileWriter("data/userCosineSimilarities.csv");
        userSimilarities = new HashMap();
        for (String i : testUsers) {
            System.out.println("User " + i);
            writer.append(i + ",");
            Map<String, Double> temp = new HashMap();
            for (int j = 1; j <= 15374; j++) {
                Map<String, Double> veci = getRatingDoubleVector(i);
                Map<String, Double> vecj = getRatingDoubleVector("" + j);
                double similarityValue = Utility.CosineSimilarity(veci, vecj, COSINE_SHRINK_USERSIM);

                if (similarityValue != 0) {
                    temp.put("" + j, similarityValue);
                    writer.append(j + "/" + similarityValue + " ");
                }
            }
            writer.write(System.getProperty("line.separator"));
            writer.flush();
            userSimilarities.put(i, temp);
        }
        writer.flush();
        writer.close();
        System.out.println("User similarities computed!");
    }

    private void computeUsersPearsonSimilarities() throws IOException {
        FileWriter writer = new FileWriter("data/userPearsonSimilarities.csv");
        userSimilarities = new HashMap();
        for (String i : testUsers) {
            System.out.println("User " + i);
            writer.append(i + ",");
            Map<String, Double> temp = new HashMap();
            for (int j = 1; j <= 15374; j++) {
                Map<String, Double> veci = getRatingDoubleVector(i);
                Map<String, Double> vecj = getRatingDoubleVector("" + j);
                double similarityValue = getPearsonCorrelation(i, "" + j, PEARSON_SHRINK_USERSIM);

                if (similarityValue != 0) {
                    temp.put("" + j, similarityValue);
                    writer.append(j + "/" + similarityValue + " ");
                }
            }
            writer.write(System.getProperty("line.separator"));
            writer.flush();
            userSimilarities.put(i, temp);
        }
        writer.flush();
        writer.close();
        System.out.println("User similarities computed!");
    }

    private void computeUsersSimpleSimilarities() throws IOException {
        FileWriter writer = new FileWriter("data/userSimpleSimilarities.csv");
        userSimilarities = new HashMap();
        for (String i : testUsers) {
            System.out.println("User " + i);
            writer.append(i + ",");
            Map<String, Double> temp = new HashMap();
            for (int j = 1; j <= 15374; j++) {
                Map<String, Double> veci = getRatingDoubleVector(i);
                Map<String, Double> vecj = getRatingDoubleVector("" + j);
                double similarityValue = Utility.simpleSimilarity(veci, vecj);

                if (similarityValue != 0) {
                    temp.put("" + j, similarityValue);
                    writer.append(j + "/" + similarityValue + " ");
                }
            }
            writer.write(System.getProperty("line.separator"));
            writer.flush();
            userSimilarities.put(i, temp);
        }
        writer.flush();
        writer.close();
        System.out.println("User similarities computed!");
    }

    private void loadUsersSimilarities() {
        userSimilarities = new HashMap();
        List<String[]> rawCsv = Utility.readCsv("data/userSimpleSimilarities.csv");
        for (String[] csvLine : rawCsv) {
            String user = csvLine[0];
            Map<String, Double> temp = new HashMap();
            for (int i = 1; i < csvLine.length; i = i + 2) {
                temp.put(csvLine[i], Double.parseDouble(csvLine[i + 1]));
            }
            userSimilarities.put(user, temp);
        }
        userPearsonSimilarities = new HashMap();
        List<String[]> rawCsv2 = Utility.readCsv("data/userPearsonSimilarities.csv");
        for (String[] csvLine : rawCsv) {
            String user = csvLine[0];
            Map<String, Double> temp = new HashMap();
            for (int i = 1; i < csvLine.length; i = i + 2) {
                temp.put(csvLine[i], Double.parseDouble(csvLine[i + 1]));
            }
            userPearsonSimilarities.put(user, temp);
        }
        System.out.println("User similarities loaded!");
    }

    public List<String> getKNN(String userU) {
        List<String> knn = new ArrayList();
        for (Map.Entry<String, Double> entry : Utility.findGreatest(userSimilarities.get(userU), KNN)) {
            knn.add(entry.getKey());
        }
        return knn;
    }

    public Map<String, String> getAllKNN(String userU) {
        Map<String, String> knn = new HashMap();
        for (Map.Entry<String, Double> entry : userSimilarities.get(userU).entrySet()) {
            knn.put(entry.getKey(), "");
        }
        return knn;
    }

    private void computeItemsAverages() {
        allItemsAverages = new HashMap();
        for (Map.Entry<String, Map<String, Integer>> entry : fasterUrmByItems.entrySet()) {
            int sum = 0;
            for (Map.Entry<String, Integer> entry2 : entry.getValue().entrySet()) {
                sum += entry2.getValue();
            }
            double avg = (double) sum / (entry.getValue().size() + AVG_SHRK);
            allItemsAverages.put(entry.getKey(), avg);
        }
        topAverages = Utility.findGreatest(allItemsAverages, allItemsAverages.size());
    }

    public List<String> getTopAverageForUser(String userId) {
        List<String> topAvgs = new ArrayList();
        for (int i = (topAverages.size() - 1); i >= 0; i--) {
            if (!alreadyWatched(userId, topAverages.get(i).getKey())) {
                topAvgs.add(topAverages.get(i).getKey());
            }
            if (topAvgs.size() == 5) {
                return topAvgs;
            }
        }
        return topAvgs;
    }

    private Map<String, Double> getRatingDoubleVector(String i) {
        Map<String, Double> result = new HashMap();
        if (fasterUrm.get(i) != null) {
            for (Map.Entry<String, Integer> entry : fasterUrm.get(i).entrySet()) {
                result.put(entry.getKey(), (double) entry.getValue());
            }
        }
        return result;
    }

    private void loadNTopPop(int TOP_POP) {
        nTopPop = new ArrayList();
        String currentItem;
        String lastItem = "";
        int count = 0;
        Map<String, Integer> temp = new HashMap();
        List<String[]> rawCsv = Utility.readCsv("data/train.csv");
        for (String[] csvLine : rawCsv) {
            currentItem = csvLine[1];
            if (!(currentItem.equals(lastItem)) && !("".equals(lastItem))) {
                temp.put(lastItem, count);
                count = 0;
            } else {
                count++;
            }
            lastItem = currentItem;
        }
        temp.put(lastItem, count);

        List<Map.Entry<String, Integer>> ordered = Utility.findGreatest(temp, TOP_POP);
        for (int i = (ordered.size() - 1); i >= 0; i--) {
            nTopPop.add(ordered.get(i).getKey());
        }
        for (String x : nTopPop) {
            System.out.println(x);
        }
    }

    public List<UrmRow> getUrm() {
        return urm;
    }

    public Map<String, List<String>> getWatchedMovies() {
        return watchedMovies;
    }

    public Map<String, Map<String, Double>> getIcm() {
        return icm;
    }

    public List<String> getTestUsers() {
        return testUsers;
    }

    public List<String> getAllFeaturedMovies() {
        return allFeaturedMovies;
    }

    public String[] getTopAvg() {
        return topAvg;
    }

    public Map<String, Double> getAllUsersAverages() {
        return allUsersAverages;
    }

    public Map<String, Map<String, Double>> getUserSimilarities() {
        return userSimilarities;
    }

    public Map<String, Map<String, Double>> getUserPearsonSimilarities() {
        return userPearsonSimilarities;
    }

    public List<String> getnTopPop() {
        return nTopPop;
    }

}
