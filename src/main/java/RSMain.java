
import it.polimi.recsys.recsys2k15.model.DataSet;
import it.polimi.recsys.recsys2k15.utils.Utility;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author paologuglielmino
 */
public class RSMain {

    public static void main(String[] args) throws IOException {
        //Load data in memory
        DataSet ds = new DataSet();

        //CONTENT BASED FILTERING
        //latestContentBasedFiltering(ds, "data/results/CBF.csv", "data/results/CBF_P.csv");
        //USER BASED COLLABORATIVE FILTERING
        //userBasedFiltering(ds, "data/results/UBF_SS.csv", "data/results/UBF_SS_P.csv");
        
        algoritmoFinale(ds, "data/test.csv", "data/results/ibrido_finale.csv", 0.3, 0.7, 15);
    }

    /**
     * Interseca due csv e trova gli elementi in comune per ogni csv
     *
     * @param path1
     * @param path2
     */
    private static void intersectTwoCsv(String path1, String path2) {
        HashMap<Integer, String[]> recMoviesCsv1 = new HashMap<>();
        HashMap<Integer, String[]> recMoviesCsv2 = new HashMap<>();
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        String csvSplitBy2 = " ";
        try {
            br = new BufferedReader(new FileReader(path1));
            String[] csvLine;
            String[] items;
            int added = 0;
            while ((line = br.readLine()) != null) {
                csvLine = line.split(",");
                if (!csvLine[0].equals("userId")) {
                    items = csvLine[1].split(" ");
                    recMoviesCsv1.put(Integer.parseInt(csvLine[0]), items);
                }
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        Map<Integer, String[]> recMoviesCsv1_sort = new TreeMap<Integer, String[]>(recMoviesCsv1);
        try {
            br = new BufferedReader(new FileReader(path2));
            String[] csvLine;
            String[] items;
            int added = 0;
            while ((line = br.readLine()) != null) {
                csvLine = line.split(",");
                if (!csvLine[0].equals("userId")) {
                    items = csvLine[1].split(" ");
                    recMoviesCsv2.put(Integer.parseInt(csvLine[0]), items);
                }
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        Map<Integer, String[]> recMoviesCsv2_sort = new TreeMap<Integer, String[]>(recMoviesCsv2);
        Map<Integer, String[]> recMoviesCsv2_completo = new HashMap<>();
        String[] temp = new String[5];
        int c = 0;
        int filmInComune = 0;
        //Faccio l' intersezione
        for (Map.Entry<Integer, String[]> entry1 : recMoviesCsv1_sort.entrySet()) {
            c = 0;
            temp = new String[5];
            for (int i = 0; i < 5; i++) {
                recMoviesCsv2_completo.put(entry1.getKey(), temp);
                if (Arrays.asList(recMoviesCsv2_sort.get(entry1.getKey())).contains(entry1.getValue()[i])) {
                    temp[c] = entry1.getValue()[i];
                    recMoviesCsv2_completo.put(entry1.getKey(), temp);
                    filmInComune++;
                    c++;
                }
            }
        }
        Map<Integer, String[]> recMoviesCsv2_completo_sort = new TreeMap<Integer, String[]>(recMoviesCsv2_completo);
        for (Map.Entry<Integer, String[]> entry : recMoviesCsv2_completo_sort.entrySet()) {
            System.out.print(entry.getKey() + ",");
            for (int i = 0; i < 5; i++) {
                try {
                    if (!entry.getValue()[i].equals("null")) {
                        System.out.print(entry.getValue()[i] + " ");
                    }
                    //System.out.print(entry.getValue()[i] + "-1 ");
                } catch (Exception e) {
                    //System.out.print(entry.getValue()[i] + "-0 ");
                }
            }
            System.out.println("");
        }
        System.out.print("Ci sono " + filmInComune + " su un totale di " + 5 * 4196 + "- dovrei inserire ancora: " + (5 * 4196 - filmInComune) + " film");
    }

    /**
     * User Based Collaborative Filtering SCORE on public leaderboard 301
     *
     * @param ds
     * @param resultPath
     * @param resultPath2
     * @throws IOException
     */
    private static void userBasedFiltering(DataSet ds, String resultPath, String resultPath2) throws IOException {
        FileWriter writer = new FileWriter(resultPath);
        FileWriter writer2 = new FileWriter(resultPath2);
        //For each user u in the test set
        for (String userU : ds.getTestUsers()) {
            System.out.println("Processing user " + userU);
            writer.append(userU + ",");
            writer2.append(userU + ",");
            Map<String, Double> predictedRatings = new HashMap();
            //List<String> knnUsers = ds.getKNN(userU, true);
            Map<String, String> knnUsers = ds.getAllKNN(userU);
            //For each movie j in the dataset
            for (int movieJ = 1; movieJ <= 37142; movieJ++) {
                //Which has not been watched by user u
                if (!ds.alreadyWatched(userU, "" + movieJ)) {
                    double sum = 0;
                    double sum2 = 0;
                    double numPearson = 0;
                    double denPearson = 0;
                    for (Map.Entry<String, String> userV : knnUsers.entrySet()) {
                        double wuv = ds.getUserSimilarities().get(userU).get(userV.getKey());
                        sum += wuv * (ds.getSpecificRating(userV.getKey(), "" + movieJ) - ds.getAllUsersAverages().get(userV.getKey()));
                        sum2 += Math.abs(wuv);
                        double simp = ds.getUserPearsonSimilarities().get(userU).get(userV.getKey());
                        numPearson += simp * (ds.getSpecificRating(userV.getKey(), "" + movieJ) - ds.getAllUsersAverages().get(userV.getKey()));
                        denPearson += Math.abs(simp);
                    }
                    double predictedRatingUJ = ds.getAllUsersAverages().get(userU) + (1 / (sum2 + ds.PREDICTED_SHRINK_UBF)) * sum;
                    double predictedRatingUJWithPearson = numPearson / (denPearson + ds.PREDICTED_SHRINK_UBF);
                    //I keep only movies not in N TopPop
                    if (!(ds.getnTopPop().contains("" + movieJ))) {
                        predictedRatings.put("" + movieJ, predictedRatingUJ /*+ predictedRatingUJWithPearson*/);
                    }
                }
            }
            List<Map.Entry<String, Double>> recommendations = Utility.findGreatest(predictedRatings, 10);
            for (int i = (recommendations.size() - 1); i >= 0; i--) {
                Map.Entry<String, Double> rec = recommendations.get(i);
                writer.append(rec.getKey() + " ");
                writer2.append(rec.getKey() + "/" + rec.getValue() + " ");
            }
            System.out.println("User " + userU + " done.");
            writer.write(System.getProperty("line.separator"));
            writer2.write(System.getProperty("line.separator"));
            writer.flush();
            writer2.flush();
        }
        writer.flush();
        writer2.flush();
        writer.close();
        writer2.close();
    }

    /**
     * User Based Collaborative Filtering for a single user
     *
     * @param ds
     * @param userU
     * @return
     */
    private static Map<String, Double> userBasedFilteringForUserU(DataSet ds, String userU) {
        Map<String, Double> predictedRatings = new HashMap();
        Map<String, String> knnUsers = ds.getAllKNN(userU);
        //For each movie j in the dataset
        for (int movieJ = 1; movieJ <= 37142; movieJ++) {
            //Which has not been watched by user u
            if (!ds.alreadyWatched(userU, "" + movieJ)) {
                double sum = 0;
                double sum2 = 0;
                double numPearson = 0;
                double denPearson = 0;
                for (Map.Entry<String, String> userV : knnUsers.entrySet()) {
                    double wuv = ds.getUserSimilarities().get(userU).get(userV.getKey());
                    sum += wuv * (ds.getSpecificRating(userV.getKey(), "" + movieJ) - ds.getAllUsersAverages().get(userV.getKey()));
                    sum2 += Math.abs(wuv);
                    double simp = ds.getUserPearsonSimilarities().get(userU).get(userV.getKey());
                    numPearson += simp * (ds.getSpecificRating(userV.getKey(), "" + movieJ) - ds.getAllUsersAverages().get(userV.getKey()));
                    denPearson += Math.abs(simp);
                }
                double predictedRatingUJ = ds.getAllUsersAverages().get(userU) + (1 / (sum2 + ds.PREDICTED_SHRINK_UBF)) * sum;
                double predictedRatingUJWithPearson = numPearson / (denPearson + ds.PREDICTED_SHRINK_UBF);
                //I keep only movies not in N TopPop
                if (!(ds.getnTopPop().contains("" + movieJ))) {
                    predictedRatings.put("" + movieJ, predictedRatingUJ);
                }
            }
        }
        return predictedRatings;
    }

    /**
     * Content Based Filtering SCORE on public leaderboard 570
     *
     * @param ds
     * @param resultPath
     * @param resultPath2
     * @throws IOException
     */
    private static void latestContentBasedFiltering(DataSet ds, String resultPath, String resultPath2) throws IOException {
        System.out.println("Starting new CBF");
        FileWriter writer = new FileWriter(resultPath);
        FileWriter writer2 = new FileWriter(resultPath2);
        //For each user in the test set
        int greater = 0;
        for (String testUser : ds.getTestUsers()) {
            System.out.println("Processing user: " + testUser);
            Map<String, Double> predictedRatings = new HashMap();
            //Analyze watched movies and compute a predicted rating for a non-watched similar movie
            System.out.println("User " + testUser + " has watched " + ds.getWatchedMovies().get(testUser).size() + " movies.");
            for (String unWatchedMovie : ds.getAllFeaturedMovies()) {
                if (!(ds.alreadyWatched(testUser, unWatchedMovie))) {
                    double numerator = 0.0;
                    double denominator = 0.0;
                    for (String watchedMovie : ds.getWatchedMovies().get(testUser)) {
                        try {
                            double similarity = Utility.simpleSimilarity(ds.getIcm().get(unWatchedMovie), ds.getIcm().get(watchedMovie));
                            numerator += similarity * ds.getSpecificRating(testUser, watchedMovie);
                            denominator += similarity;
                        } catch (NullPointerException e) {
                            numerator += 0.0;
                            denominator += 0.0;
                        }
                    }
                    double predictedRating = numerator / (denominator + ds.PREDICTED_SHRINK_CBF);
                    predictedRatings.put(unWatchedMovie, predictedRating);
                }
            }
            //Find 5 recommendations among the movies with highest predicted rating
            List<Map.Entry<String, Double>> recommendations = Utility.findGreatest(predictedRatings, 10);
            writer.append(testUser + ",");
            writer2.append(testUser + ",");

            for (int i = (recommendations.size() - 1); i >= 0; i--) {
                Map.Entry<String, Double> rec = recommendations.get(i);
                writer.append(rec.getKey() + " ");
                //if(ds.getWatchedMovies().get(testUser).size()<50){
                writer2.append(rec.getKey() + "/" + rec.getValue() + " ");
                //}
                //else{
                //writer2.append(rec.getKey() + "-" + 0+" ");
                //greater++;
                //}
            }
            writer.write(System.getProperty("line.separator"));
            writer2.write(System.getProperty("line.separator"));
            writer.flush();
            writer2.flush();
        }
        System.out.println(greater);
        writer.flush();
        writer2.flush();
        writer.close();
        writer2.close();
    }

    /**
     * Content Based Filtering for a single user
     *
     * @param ds
     * @param testUser
     * @return
     */
    private static Map<String, Double> latestContentBasedFilteringForUserU(DataSet ds, String testUser) {
        Map<String, Double> predictedRatings = new HashMap();
        //Analyze watched movies and compute a predicted rating for a non-watched similar movie
        for (String unWatchedMovie : ds.getAllFeaturedMovies()) {
            if (!(ds.alreadyWatched(testUser, unWatchedMovie))) {
                double numerator = 0.0;
                double denominator = 0.0;
                for (String watchedMovie : ds.getWatchedMovies().get(testUser)) {
                    try {
                        //double similarity = Utility.CosineSimilarity(ds.getIcm().get(unWatchedMovie), ds.getIcm().get(watchedMovie), 8);                            
                        double similarity = Utility.simpleSimilarity(ds.getIcm().get(unWatchedMovie), ds.getIcm().get(watchedMovie));
                        //System.out.println("Cosine : "+similarity);
                        //System.out.println("Gabriele sim: "+Utility.simpleSimilarity(ds.getIcm().get(unWatchedMovie), ds.getIcm().get(watchedMovie)));
                        numerator += similarity * ds.getSpecificRating(testUser, watchedMovie);
                        denominator += similarity;
                    } catch (NullPointerException e) {
                        numerator += 0.0;
                        denominator += 0.0;
                    }
                }
                double predictedRating = numerator / (denominator + ds.PREDICTED_SHRINK_CBF);
                predictedRatings.put(unWatchedMovie, predictedRating);
            }
        }
        return predictedRatings;
    }

    /**
     * Hybridization algorithm, joins predictions coming from the two algorithms
     * use the predicted ratings value as a weight to do the final
     * recommendation
     *
     * @param ds
     * @param testUsersFile
     * @param resultPath
     * @param alfa CBF weight
     * @param beta UBF weight
     * @param rat
     * @throws IOException
     */
    private static void algoritmoFinale(DataSet ds, String testUsersFile, String resultPath, double alfa, double beta, int rat) throws IOException {
        FileWriter writer = new FileWriter(resultPath);
        writer.append("userId,testItems");
        writer.write(System.getProperty("line.separator"));
        List<String[]> rawBase = Utility.readCsv(testUsersFile);
        for (String[] csvLine : rawBase) {
            String userId = csvLine[0];
            System.out.println("User: " + userId);
            writer.append(userId + ",");

            int watched = ds.getWatchedMovies().get(userId).size();
            if (watched > rat) {
                Map<String, Double> cbfPredictions = latestContentBasedFilteringForUserU(ds, userId);
                List<Map.Entry<String, Double>> list = Utility.findGreatest(cbfPredictions, 5);
                for (int i = (list.size() - 1); i >= 0; i--) {
                    writer.append(list.get(i).getKey() + " ");
                }
                writer.write(System.getProperty("line.separator"));
                writer.flush();
            } else {
                Map<String, Double> cbfPredictions = latestContentBasedFilteringForUserU(ds, userId);
                Map<String, Double> ubfPredictions = userBasedFilteringForUserU(ds, userId);
                //Riscalo i pesi dati dal CBF di un fattore pari ad alfa
                for (Map.Entry<String, Double> e : cbfPredictions.entrySet()) {
                    e.setValue(alfa * e.getValue());
                }
                //Riscalo i pesi dati dal UBF di un fattore pari a beta
                for (Map.Entry<String, Double> e : ubfPredictions.entrySet()) {
                    e.setValue(beta * e.getValue());
                }
                //Sommo i pesi comuni, uso la Map dello UBF per raccogliere tutti i dati
                for (Map.Entry<String, Double> e : cbfPredictions.entrySet()) {
                    Double v = ubfPredictions.get(e.getKey());
                    if (v == null) {
                        ubfPredictions.put(e.getKey(), e.getValue());
                    } else {
                        ubfPredictions.put(e.getKey(), (v + e.getValue()));
                    }
                }
                List<Map.Entry<String, Double>> list = Utility.findGreatest(ubfPredictions, 5);
                for (int i = (list.size() - 1); i >= 0; i--) {
                    writer.append(list.get(i).getKey() + " ");
                }
                writer.write(System.getProperty("line.separator"));
                writer.flush();
            }
        }
        writer.flush();
        writer.close();
    }
}
