
import it.polimi.recsys.recsys2k15.model.DataSet;
import it.polimi.recsys.recsys2k15.model.ItemRating;
import it.polimi.recsys.recsys2k15.model.SimilarCouple;
import it.polimi.recsys.recsys2k15.utils.Utility;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.svd.Factorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDPlusPlusFactorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDRecommender;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author paologuglielmino
 */
public class OldAlgorithms {

    /**
     * CBF prima versione, score = 393
     *
     * @param ds
     * @param resultPath
     * @param resultPath2
     * @throws IOException
     */
    private static void contentBasedFiltering(DataSet ds, String resultPath, String resultPath2) throws IOException {
        System.out.println("Starting CBF");
        FileWriter writer = new FileWriter(resultPath);
        FileWriter writer2 = new FileWriter(resultPath2);
        writer.append("userId,testItems");
        writer2.append("userId,testItems");
        writer.write(System.getProperty("line.separator"));
        writer2.write(System.getProperty("line.separator"));
        //For each user in the test set
        for (String testUser : ds.getTestUsers()) {
            System.out.println("Processing user: " + testUser);
            Map<String, Double> predictedRatings = new HashMap();
            //Analyze watched movies and compute a predicted rating for a non-watched similar movie
            System.out.println("User " + testUser + " has watched " + ds.getWatchedMovies().get(testUser).size() + " movies.");
            for (String watchedMovie : ds.getWatchedMovies().get(testUser)) {
                //Consider only watched movies with rating ge 6
                if (ds.getSpecificRating(testUser, watchedMovie) >= ds.RATING_THRESHOLD) {
                    //List of similarities between watched movie and all the otehrs non-watched
                    List<SimilarCouple> similarities = new ArrayList();
                    for (String secondMovie : ds.getAllFeaturedMovies()) {
                        if (!(ds.alreadyWatched(testUser, secondMovie))) {
                            double similarity = Utility.CosineSimilarity(ds.getIcm().get(watchedMovie), ds.getIcm().get(secondMovie), ds.COSINE_SHRINK);
                            if (similarity >= ds.SIMILARITY_THRESHOLD) {
                                similarities.add(new SimilarCouple(watchedMovie, secondMovie, similarity));
                            }
                        }
                    }
                    //Rating prediction for similar movies
                    double rating = ds.getSpecificRating(testUser, watchedMovie);
                    for (SimilarCouple simCoup : similarities) {
                        double predictedRating = simCoup.getSimilarity() * rating;
                        ItemRating similarMovie = new ItemRating(simCoup.getEl2(), predictedRating);
                        predictedRatings.put(similarMovie.getItemId(), similarMovie.getPredictedRating());
                    }
                }
            }
            //Find 5 recommendations among the movies with highest predicted rating
            List<Map.Entry<String, Double>> recommendations = Utility.findGreatest(predictedRatings, 5);
            writer.append(testUser + ",");
            writer2.append(testUser + ",");
            for (int i = (recommendations.size() - 1); i >= 0; i--) {
                Map.Entry<String, Double> rec = recommendations.get(i);
                writer.append(rec.getKey() + " ");
                writer2.append(rec.getKey() + "/" + rec.getValue() + " ");
            }
            writer.write(System.getProperty("line.separator"));
            writer2.write(System.getProperty("line.separator"));
            writer.flush();
            writer2.flush();
        }
        writer.flush();
        writer2.flush();
        writer.close();
        writer2.close();
    }

    /**
     * CBF nuova versione, considera tutte le similarity
     *
     * @param ds
     * @param resultPath
     * @param resultPath2
     * @throws IOException
     */
    private static void newContentBasedFiltering(DataSet ds, String resultPath, String resultPath2) throws IOException {
        System.out.println("Starting new CBF");
        FileWriter writer = new FileWriter(resultPath);
        FileWriter writer2 = new FileWriter(resultPath2);
        writer.append("userId,testItems");
        writer2.append("userId,testItems");
        writer.write(System.getProperty("line.separator"));
        writer2.write(System.getProperty("line.separator"));
        //For each user in the test set
        for (String testUser : ds.getTestUsers()) {
            System.out.println("Processing user: " + testUser);
            Map<String, Double> predictedRatings = new HashMap();
            //Analyze watched movies and compute a predicted rating for a non-watched similar movie
            System.out.println("User " + testUser + " has watched " + ds.getWatchedMovies().get(testUser).size() + " movies.");
            for (String unWatchedMovie : ds.getAllFeaturedMovies()) {
                if (!(ds.alreadyWatched(testUser, unWatchedMovie))) {
                    double numerator = 0;
                    double denominator = 0;
                    for (String watchedMovie : ds.getWatchedMovies().get(testUser)) {
                        double similarity = Utility.CosineSimilarity(ds.getIcm().get(unWatchedMovie), ds.getIcm().get(watchedMovie), ds.COSINE_SHRINK);
                        numerator += similarity * ds.getSpecificRating(testUser, watchedMovie);
                        denominator += similarity;
                    }
                    if (denominator == 0) {
                        denominator = 1;
                    }
                    double predictedRating = numerator / (denominator + ds.CBF_SHRINK);
                    predictedRatings.put(unWatchedMovie, predictedRating);
                }
            }
            //Find 5 recommendations among the movies with highest predicted rating
            List<Map.Entry<String, Double>> recommendations = Utility.findGreatest(predictedRatings, 5);
            writer.append(testUser + ",");
            writer2.append(testUser + ",");
            for (int i = (recommendations.size() - 1); i >= 0; i--) {
                Map.Entry<String, Double> rec = recommendations.get(i);
                writer.append(rec.getKey() + " ");
                writer2.append(rec.getKey() + "/" + rec.getValue() + " ");
            }
            writer.write(System.getProperty("line.separator"));
            writer2.write(System.getProperty("line.separator"));
            writer.flush();
            writer2.flush();
        }
        writer.flush();
        writer2.flush();
        writer.close();
        writer2.close();
    }

    /**
     * Persionalized Top Average (Rimuovo i film che l'utente ha eventualmente
     * gi visto dalle recommendations), score = 37
     *
     * @param ds
     * @param destPath
     * @throws IOException
     */
    private static void personalizedTopAverage(DataSet ds, String destPath) throws IOException {
        FileWriter writer = new FileWriter(destPath);
        writer.append("userId,testItems");
        writer.write(System.getProperty("line.separator"));
        for (String user : ds.getTestUsers()) {
            writer.append(user + ",");
            List<String> recommendations = ds.getTopAverageForUser(user);
            for (String rec : recommendations) {
                writer.append(rec + " ");
            }
            writer.write(System.getProperty("line.separator"));
        }
        writer.flush();
        writer.close();
    }

    /**
     * SVD++ Algorithm implemented with Mahout
     *
     * @param destFile
     * @param features
     * @param iterations
     * @throws IOException
     */
    private static void MahoutSVD(DataSet ds, String destFile, int features, int iterations) throws IOException {
        //Prova Mahout SVD Factorization
        DataModel dataModel = new FileDataModel(new File("data/train_by_users.csv"));

        FileWriter writer = new FileWriter(destFile);
        writer.append("userId,testItems");
        writer.write(System.getProperty("line.separator"));

        try {
            //Try with different factorizers
            //Factorizer factorizer = new ExpectationMaximizationSVDFactorizer(dataModel, features, iterations);
            Factorizer factorizer = new SVDPlusPlusFactorizer(dataModel, features, iterations);

            System.out.println("Building recommender...");
            SVDRecommender recommender = new SVDRecommender(dataModel, factorizer);
            System.out.println("Recommender built.");

            List<String> testUsersList = ds.getTestUsers();

            for (int i = 1; i <= 15374; i++) {
                if (testUsersList.contains("" + i)) {
                    System.out.println("Processing user " + i);
                    int recsDone = 0;
                    List<RecommendedItem> recommendations = recommender.recommend(i, 20);
                    writer.append(i + ",");
                    for (RecommendedItem recommendation : recommendations) {
                        if (!(ds.getnTopPop().contains("" + recommendation.getItemID())) && recsDone < 5) {
                            writer.append("" + recommendation.getItemID()/* + "-" + recommender.estimatePreference(i, recommendation.getItemID())*/ + " ");
                            recsDone++;
                        }
                    }
                    writer.write(System.getProperty("line.separator"));
                }
            }
            writer.flush();
            writer.close();
        } catch (TasteException ex) {
            Logger.getLogger(RSMain.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
